<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
<h1>
    Users
</h1>
<table>
    <thead>
    <tr>
        <th>
            Role
        </th>
        <th>
            Email
        </th>
        <th>
            Password
        </th>
    </tr>
    </thead>
    <c:forEach var="user" items="${users}">
        <tr>
            <td>
                    ${user.role}
            </td>
            <td>
                    ${user.email}
            </td>
            <td>
                    ${user.password}
            </td>
            <td>
                <form:form action="/users/delete/${user.id}" method="post">
                    <button type="submit">Delete</button>
                </form:form>
            </td>
            <td>
                <form:form action="/users/update/${user.id}" method="get">
                    <button type="submit">Update</button>
                </form:form>
            </td>
        </tr>
    </c:forEach>
    <div>
        <h2>
            User Creation
        </h2>
        <form:form action="/users/create" method="post" modelAttribute="user">
            <label>
                Email:
                <form:input path="email"/>
            </label>
            <label>
                Password:
                <form:input path="password"/>
            </label>
            <label>
                Role:
                <form:input path="role"/>
            </label>
            <button type="submit">Create</button>
        </form:form>

    </div>
</table>
</body>
</html>
