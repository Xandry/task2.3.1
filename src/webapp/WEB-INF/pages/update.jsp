<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update User</title>
</head>
<body>
<h1>Update user</h1>
<div>
    <form:form action="/users/update/${id}" method="post" modelAttribute="user">
        <label>
            Email:
            <form:input path="email"/>
        </label>
        <label>
            Password:
            <form:input path="password"/>
        </label>
        <label>
            Role:
            <form:input path="role"/>
        </label>
        <button type="submit">Update</button>
    </form:form>
</div>
</body>
</html>
