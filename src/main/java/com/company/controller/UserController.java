package com.company.controller;

import com.company.model.User;
import com.company.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public String getUsers(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("user", new User());
        return "users";
    }

    @PostMapping("/users/create")
    public String createUser(@ModelAttribute("user") User user) {
        userService.addUser(user);
        return "redirect: /users";
    }

    @PostMapping("/users/delete/{id}")
    public String deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return "redirect: /users";
    }

    @GetMapping("/users/update/{id}")
    public String getUpdateUser(@PathVariable Long id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("user", new User());
        return "update";
    }

    @PostMapping("/users/update/{id}")
    public String updateUser(@ModelAttribute("user") User user,
                             @PathVariable Long id) {
        user.setId(id);
        userService.updateUser(user);
        return "redirect: /users";
    }


}
