package com.company.dao;

import com.company.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Slf4j
@Repository
public class UserDaoHibernateImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public User getUser(long id) {
        return entityManager.find(User.class, id);
    }


    @Override
    public List<User> getAllUsers() {
        TypedQuery<User> query = entityManager.createQuery("from User", User.class);
        return query.getResultList();
    }


    @Override
    public void deleteUser(long id) {
        User user = entityManager.find(User.class, id);
        entityManager.remove(user);
        log.debug("Deleted: " + id);
    }


    @Override
    public void deleteAll() {
        entityManager.createQuery("delete from User").executeUpdate();
        log.debug("Deleted all users");
    }


    @Override
    public void addUser(User user) {
        entityManager.persist(user);
        log.debug("Saved: " + user.getEmail());
    }


    @Override
    public void updateUser(User newUser) {
        entityManager.merge(newUser);
        entityManager.flush();
        log.debug("Updated: " + newUser.getEmail());
    }
}
