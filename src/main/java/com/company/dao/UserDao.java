package com.company.dao;

import com.company.model.User;

import java.util.List;

public interface UserDao {

    User getUser(long id);

    List<User> getAllUsers();

    void deleteUser(long id);

    void deleteAll();

    void addUser(User user);

    void updateUser(User user);

}
